const express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const path = require("path");
server = express();
const { data } = require("./names");
const Port = process.env.Port || 8080;

const static = path.join(__dirname, "public");
const views = path.join(__dirname, "views");
server.set("view engine", "ejs");
server.use(express.static(static));
server.set("views", views);

server.use(
  bodyParser.urlencoded({
    extended: false
  })
);
server.use(morgan("tiny"));
server.use(bodyParser.json());

const allNames = data.map(items => {
  return items.fName;
});

server.get("/", (req, res) => {
  res.render("index");
});

server.get("/autocomplite", (req, res) => {
  res.json({ allNames });
});

server.listen(Port, () => console.log(`Server on port:${Port}`));
